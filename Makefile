install:
	@poetry install
	@cd djangovue/plugins; npm install

test:
	@poetry run pytest --cov=djangovue --cov-report=html

patch:
	@poetry run bumpversion patch

bundle:
	@cd djangovue/plugins;npm run bundle

black:
	@poetry run black djangovue
