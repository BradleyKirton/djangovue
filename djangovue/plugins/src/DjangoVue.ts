import axios from "axios";
import { set } from "lodash";


/*
 * Set the data attribute on the Vue instance for a checkbox input.
 * @param {HTMLInputElement} element - An html input element of type "checkbox"
 * @param {string} modelPath - A string path for the v-model binding
 * @param {object} data - An object to which the value will be assigned
 */
function handleCheckBoxInput(element: HTMLInputElement, modelPath: string, data: object) {
	set(data, modelPath, element.checked);
}

/*
 * Set the data attribute on the Vue instance for a radio input.
 * @param {HTMLInputElement} element - An html input element of type "radio"
 * @param {string} modelPath - A string path for the v-model binding
 * @param {object} data - An object to which the value will be assigned
 */
function handleRadioInput(element: HTMLInputElement, modelPath: string, data: object) {
	handleGenericInput(element, modelPath, data);
}

/*
 * Set the data attribute(s) on the Vue instance for a select input.
 * @param {HTMLSelectElement} element - An html select element
 * @param {string} modelPath - A string path for the v-model binding
 * @param {object} data - An object to which the value will be assigned
 */
function handleSelectInput(element: HTMLSelectElement, modelPath: string, data: object) {
	if (element.multiple == false) {
		let options: NodeListOf<HTMLOptionElement> = element.querySelectorAll("option");
		let selectedOption: HTMLOptionElement;

		options.forEach(option => {
			if (option.selected == true && selectedOption == null) {
				selectedOption = option;
			}
		});

		if (selectedOption == null) {
			selectedOption = options[0];
		}

		set(data, modelPath, selectedOption.value);
	} else {
		let slots: Array<boolean> = [];
		element.querySelectorAll("option").forEach(option => {
			if (option.selected == true) {
				slots.push(option.selected);
			}
		});

		set(data, modelPath, slots);
	}
}

/*
 * Set the data attribute on the Vue instance for any other element besised a radio, select and checkbox.
 * @param {HTMLInputElement} element - An html input element
 * @param {string} modelPath - A string path for the v-model binding
 * @param {object} data - An object to which the value will be assigned
 */
function handleGenericInput(element: HTMLInputElement, modelPath: string, data: object) {
	set(data, modelPath, element.value);
}


/*
 * Extracts the initial values for each element with a "v-model" attribute and returns an object
 * containing this data.
 * @param {HTMLElement} rootElement - The root element containing the elements to be scraped
 */
function getInitialValues(rootElement: HTMLElement) {
	const data = {};
	const elementsMapper: Array<[string, NodeListOf<Element>]> = [
		["v-model", rootElement.querySelectorAll("[v-model]")],
		["v-model.lazy", rootElement.querySelectorAll("[v-model\\.lazy]")],
		["v-model.number", rootElement.querySelectorAll("[v-model\\.number]")],
		["v-model.trim", rootElement.querySelectorAll("[v-model\\.trim]")],
	];

	elementsMapper.forEach(([attributeSelector, nodeList]) => {
		nodeList.forEach((element: HTMLInputElement) => {
		  	let modelPath = element.getAttribute(attributeSelector);
		  	let elementType = element.getAttribute("type");

		  	if (element instanceof HTMLInputElement  && elementType == "checkbox") {
		  		handleCheckBoxInput(element, modelPath, data);
		  	} else if (element instanceof HTMLInputElement  && elementType == "radio") {
		  		handleRadioInput(element, modelPath, data);
		  	} else if (element instanceof HTMLSelectElement) {
		  		handleSelectInput(element, modelPath, data);
		  	} else {
		  		handleGenericInput(element, modelPath, data);
		  	}
	  	})
	  });

	  return data;
}


/*
 * DjangoVue Vue plugin.
 *
 * This plugin configures axios for CSRF within a standard Django application
 * and sets the initial value of all elements marked with "v-model" on the Vue instance.
 */
export const DjangoVue = {
	install(Vue: any, options:  any) {
		// Setup axios
		axios.defaults.xsrfCookieName = 'csrftoken';
		axios.defaults.xsrfHeaderName = 'X-CSRFToken';
		Vue.prototype.$http = axios;

		// Add mixin
		Vue.mixin({
			data() {
				let rootElement = (
					this.$options.el instanceof Element ? this.$options.el : document.querySelector(this.$options.el)
				);

				if (rootElement == null) {
					return {};
				}

				return getInitialValues(
					document.querySelector(this.$options.el)
				);
			}
		});
	}
};
