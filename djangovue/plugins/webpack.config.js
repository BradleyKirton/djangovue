const path = require("path");

module.exports = {
    entry: "./src/DjangoVue.ts",
    output: {
        filename: "djangovue.js",
        path: path.resolve(__dirname, "../static/djangovue/"),
        library: "djangovue",
        libraryTarget: "umd",
    },
    mode: "production",
    devtool: "source-map",
    resolve: {
        extensions: [".ts", ".js", ".json"],
    },
    module: {
        rules: [
            { test: /\.tsx?$/, loader: "awesome-typescript-loader" },
            { enforce: "pre", test: /\.js$/, loader: "source-map-loader" },
        ]
    },
};
