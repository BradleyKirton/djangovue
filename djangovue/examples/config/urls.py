from django.urls import path
from example.views import base_view, login_view, forms_view, templating_view


urlpatterns = [
    path("", base_view, name="index"),
    path("login/", login_view, name="login"),
    path("forms/", forms_view, name="forms"),
    path("templating_view/", templating_view, name="templating"),
]
