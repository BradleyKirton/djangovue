from django import forms
from djangovue.forms import widgets


class UserForm(forms.Form):
    username = forms.CharField(
        max_length=30,
        widget=widgets.TextInput(
            model="username",
            modifier=widgets.TextInput.LAZY,
            attrs={":disabled": "disable"},
        ),
    )
    first_name = forms.CharField(
        max_length=30,
        widget=widgets.TextInput(
            model="user.first_name", attrs={":disabled": "disable"}
        ),
    )
    last_name = forms.CharField(
        max_length=30,
        widget=widgets.TextInput(
            model="user.last_name", attrs={":disabled": "disable"}
        ),
    )
    gender = forms.ChoiceField(
        choices=[("male", "Male"), ("female", "Female")],
        widget=widgets.RadioSelect(
            attrs={"v-model": "user.gender", ":disabled": "disable"}
        ),
    )
    example_select_1 = forms.ChoiceField(
        choices=[(1, "One"), (2, "Two")],
        widget=widgets.Select(attrs={":disabled": "disable"}),
    )
    example_select_2 = forms.ChoiceField(
        choices=[(1, "One"), (2, "Two")],
        widget=widgets.SelectMultiple(attrs={":disabled": "disable"}),
    )
    bio = forms.CharField(
        max_length=30,
        widget=widgets.Textarea(model="user.bio", attrs={":disabled": "disable"}),
    )
    null_boolean_select = forms.BooleanField(widget=widgets.NullBooleanSelect())
    disable = forms.BooleanField(
        required=False, widget=widgets.CheckboxInput(model="disable")
    )
