from typing import Dict
from django.views.generic import TemplateView
from django.views.generic import View
from django.http import JsonResponse
from django.urls import reverse
from pygments import highlight
from pygments.lexers import DjangoLexer
from pygments.formatters import HtmlFormatter
from example.forms import UserForm

import json
import secrets
import pathlib


random = secrets.SystemRandom()


class BaseView(TemplateView):
    """django-vue example view."""

    template_name = "example/base.html"


class TemplatingView(TemplateView):
    """django-vue example view."""

    template_name = "example/templating.html"

    def get_context_data(self, **kwargs):
        """"""

        context = super().get_context_data(**kwargs)
        context = {"django": "Hello django", "vue": "Hello vue"}

        path = pathlib.Path(__file__).parent
        path = path.joinpath("templates", "example", "templating.html")

        with open(path) as f:
            template = f.read()

        template = template.replace(r"\t", "  ")
        template = template.replace(r"    ", "  ")
        formatter = HtmlFormatter()
        context["styles"] = formatter.get_style_defs()
        context["template"] = highlight(template, DjangoLexer(), formatter)

        return {"context": context}


class LoginView(View):
    """fake login view."""

    def post(self, request, **kwargs):
        """"""

        credentials = json.loads(request.body)

        conds = [credentials["username"] == "test", credentials["password"] == "test"]

        if all(conds):
            return JsonResponse(
                {
                    "username": credentials["username"],
                    "password": credentials["password"],
                    "token": secrets.token_urlsafe(),
                }
            )
        else:
            return JsonResponse({}, status=400)


class FormsView(TemplateView):
    """django-vue example view."""

    template_name = "example/forms.html"

    def get_context_data(self, **kwargs) -> Dict:
        context = super().get_context_data(**kwargs)

        # Userform
        userform = UserForm(
            initial={
                "username": "francisw",
                "first_name": "Francis",
                "last_name": "Watson",
                "example_select_1": 2,
            }
        )

        context["userform"] = userform
        return context


# View functions
base_view = BaseView.as_view()
templating_view = TemplatingView.as_view()
login_view = LoginView.as_view()
forms_view = FormsView.as_view()
