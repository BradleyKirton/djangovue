from django import conf

import pytest


@pytest.fixture
def settings() -> str:
    return conf.settings


@pytest.fixture
def local_script() -> str:
    return "/static/test.js"


@pytest.fixture
def local_script_tag() -> str:
    return '<script src="/static/test.js"></script>'


@pytest.fixture
def local_script_tag_defered() -> str:
    return '<script src="/static/test.js" defer></script>'


@pytest.fixture
def cdn_script() -> str:
    return "https://cnd/test/test.js"


@pytest.fixture
def cdn_script_tag() -> str:
    return '<script src="https://cnd/test/test.js"></script>'


@pytest.fixture
def local_stylesheet() -> str:
    return "/static/test.css"


@pytest.fixture
def local_stylesheet_tag() -> str:
    return '<link rel="stylesheet" href="/static/test.css"></link>'


@pytest.fixture
def cdn_stylesheet() -> str:
    return "https://cnd/test/test.css"


@pytest.fixture
def cdn_stylesheet_tag() -> str:
    return '<link rel="stylesheet" href="https://cnd/test/test.css"></link>'


@pytest.fixture
def djangovue_plugin_tag() -> str:
    return '<script src="/static/djangovue/djangovue.js"></script>'
