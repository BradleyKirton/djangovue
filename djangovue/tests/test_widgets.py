from djangovue.forms.widgets import VueWidget
from unittest.mock import MagicMock


class TestVueWidget:
    """Test suite for the VueWidget."""

    def test_widget_attributes(self) -> None:
        widget = VueWidget(model="test", modifier=VueWidget.LAZY)
        assert widget._model == "test"
        assert widget._modifier == VueWidget.LAZY

    def test_vmodel_attribute(self) -> None:
        widget = VueWidget()
        assert widget.vmodel_attribute == "v-model"

    def test_vmodel_attribute_lazy(self) -> None:
        widget = VueWidget(modifier=VueWidget.LAZY)
        assert widget.vmodel_attribute == "v-model.lazy"

    def test_vmodel_attribute_trim(self) -> None:
        widget = VueWidget(modifier=VueWidget.TRIM)
        assert widget.vmodel_attribute == "v-model.trim"

    def test_vmodel_attribute_number(self) -> None:
        widget = VueWidget(modifier=VueWidget.NUMBER)
        assert widget.vmodel_attribute == "v-model.number"

    def test_modifier_choices(self) -> None:
        assert VueWidget.LAZY == "lazy"
        assert VueWidget.TRIM == "trim"
        assert VueWidget.NUMBER == "number"

    def test_modifier(self) -> None:
        widget = VueWidget(modifier=VueWidget.LAZY)
        assert widget.modifier == VueWidget.LAZY

        widget = VueWidget(modifier=VueWidget.TRIM)
        assert widget.modifier == VueWidget.TRIM

        widget = VueWidget(modifier=VueWidget.NUMBER)
        assert widget.modifier == VueWidget.NUMBER

    def test_render_with_model(self):
        widget = VueWidget(model="test", modifier=VueWidget.LAZY)
        widget.template_name = MagicMock()
        widget._render = MagicMock()
        widget.get_context = MagicMock()
        widget.render(name="name", value="value")

        assert widget.get_context.call_args[0][0] == "name"
        assert widget.get_context.call_args[0][1] == "value"
        assert widget.get_context.call_args[0][2] == {"v-model.lazy": "test"}

    def test_render_without_model(self):
        widget = VueWidget(modifier=VueWidget.LAZY)
        widget.template_name = MagicMock()
        widget._render = MagicMock()
        widget.get_context = MagicMock()
        widget.render(name="name", value="value")

        assert widget.get_context.call_args[0][0] == "name"
        assert widget.get_context.call_args[0][1] == "value"
        assert widget.get_context.call_args[0][2] == {"v-model.lazy": "name"}

    def test_render_with_attrs(self):
        widget = VueWidget(modifier=VueWidget.LAZY)
        widget.template_name = MagicMock()
        widget._render = MagicMock()
        widget.get_context = MagicMock()
        widget.render(name="name", value="value", attrs={"id": "test"})

        assert widget.get_context.call_args[0][0] == "name"
        assert widget.get_context.call_args[0][1] == "value"
        assert widget.get_context.call_args[0][2] == {
            "id": "test",
            "v-model.lazy": "name",
        }
