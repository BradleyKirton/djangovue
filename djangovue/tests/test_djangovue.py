from unittest.mock import patch
from django.test import override_settings
from djangovue.vue import (
    get_vue_setting,
    wrap_script_src,
    wrap_link_href,
    get_custom_script_tag,
    get_custom_link_tag,
    get_vuejs_script_tag,
    get_djangovue_script_tag,
)


class TestVueHelpersTestCase:
    """Test the django vue helper functions."""

    def test_get_vue_setting(self):
        assert isinstance(get_vue_setting("scripts"), dict)

    def test_wrap_script_src(self, local_script: str, local_script_tag: str):
        assert wrap_script_src(local_script) == local_script_tag

    def test_wrap_link_href(self, local_stylesheet: str, local_stylesheet_tag: str):
        assert wrap_link_href(local_stylesheet) == local_stylesheet_tag

    def test_get_custom_script_tag(self, local_script_tag: str):
        assert get_custom_script_tag("test") == local_script_tag

    def test_get_custom_script_tag_defer(self, local_script_tag_defered: str):
        with patch.dict("djangovue.vue.VUE_SETTINGS", {"defer_scripts": True}):
            assert get_custom_script_tag("test") == local_script_tag_defered

    def test_get_custom_link_tag(self, local_stylesheet_tag: str):
        assert get_custom_link_tag("test") == local_stylesheet_tag

    def test_get_vuejs_script_tag_development(self, settings):
        settings.DEBUG = True
        uri = "https://cdn.jsdelivr.net/npm/vue/dist/vue.js"
        assert get_vuejs_script_tag() == f'<script src="{uri}"></script>'

    def test_get_vuejs_script_tag_production(self, settings):
        settings.DEBUG = False
        uri = "https://cdn.jsdelivr.net/npm/vue"
        assert get_vuejs_script_tag() == f'<script src="{uri}"></script>'

    def test_get_djangovue_script_tag(self, djangovue_plugin_tag: str):
        assert get_djangovue_script_tag() == djangovue_plugin_tag
