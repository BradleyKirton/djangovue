from django.utils.html import format_html
from djangovue.templatetags.djangovue import (
    load_vuejs,
    load_djangovue_plugin,
    render_script_tag,
    render_link_tag,
    djangovue,
)


class TestDjangoVueTemplateTagTestCase:
    def test_load_vuejs_development(self, settings):
        settings.DEBUG = True
        uri = "https://cdn.jsdelivr.net/npm/vue/dist/vue.js"
        script_tag = load_vuejs()
        assert script_tag == format_html(f'<script src="{uri}"></script>')

    def test_load_vuejs_production(self, settings):
        settings.DEBUG = True
        uri = "https://cdn.jsdelivr.net/npm/vue/dist/vue.js"
        script_tag = load_vuejs()
        assert script_tag == format_html(f'<script src="{uri}"></script>')

    def test_render_script_tag(self, local_script_tag: str):
        assert render_script_tag("test") == format_html(local_script_tag)

    def test_render_link_tag(self, local_stylesheet_tag: str):
        assert render_link_tag("test"), format_html(local_stylesheet_tag)

    def test_load_djangovue_plugin(self, djangovue_plugin_tag: str):
        assert load_djangovue_plugin() == format_html(djangovue_plugin_tag)
