from djangovue.apps import DjangoVueConfig


def test_app_config():
    assert DjangoVueConfig.name == "django_vue"
