from django.apps import AppConfig


class DjangoVueConfig(AppConfig):
    name = "django_vue"
